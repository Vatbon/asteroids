#pragma once
#include "../Util.h"

#include "../drawable/Drawable.h"
#include "../drawable/Sprite.h"
#include "../logic/Movable.h"
#include "../logic/Dummy.h"

class Entity : public Drawable, Movable
{
public:
	Entity() 
	{
		sprite = new Sprite();
		this->x = 0;
		this->y = 0;
		this->dir = 0;
		this->speed = 0;
	}

	Entity(Sprite* sprite)
	{
		this->sprite = sprite;
		this->x = 0;
		this->y = 0;
		this->dir = 0;
		this->speed = 0;
	}

	Entity(Sprite *sprite, float x, float y) 
	{
		this->sprite = sprite;
		this->x = x;
		this->y = y;
		this->dir = 0;
		this->speed = 0;
	}

	~Entity() 
	{
		delete dummy;
		delete sprite;
		if (transformedLines != nullptr)
			clearList(transformedLines);
		delete transformedLines;
	}

	virtual std::list<Line*> getLines();
	float getDir();
	void setDir(float dir);
	void setSizeFactor(float factor);
	float getSizeFactor();
	void setX(float x);
	void setY(float y);
	float getX();
	float getY();
	float getSpeed();
	void setSpeed(float speed);
	virtual void move(float dt);
	bool collidesWith(Entity* ent);

protected:
	float x, y;
	float dir;
	float speed;
	Sprite* sprite;
	Dummy* dummy = nullptr;
	std::list<Line*>* transformedLines = NULL;
};

