#pragma once
#include "Entity.h"
#include "../Color.h"
#include "Bullet.h"

class Ship :
    public Entity
{
public:
    Ship(float x, float y, float dir) 
    {
        Sprite* sprite = new Sprite(0.8, 0.5);
        sprite->addLine(new Line(0, 0, 1.8, 0.5, WHITE_UINT_32));
        sprite->addLine(new Line(1.8, 0.5, 0, 1, WHITE_UINT_32));
        sprite->addLine(new Line(0, 1, .2, 0.6, WHITE_UINT_32));
        sprite->addLine(new Line(.2, 0.6, 0.2, 0.4, WHITE_UINT_32));
        sprite->addLine(new Line(0.2, 0.4, 0, 0, WHITE_UINT_32));
        sprite->setSizeFactor(20);

        this->sprite = sprite;

        this->dummy = new Dummy(0.8 * 20, 0.5 * 20, 1.8 * 20, 1 * 20);

        this->x = x;
        this->y = y;
        this->dir = dir;
        this->speed = 0;
        this->speedX = 0;
        this->speedY = 0;
        this->acceleration = 0;
    }

    void accelerate(bool b);
    void turnLeft(bool b);
    void turnRight(bool b);
    void move(float dt);
    bool isImmortal();
    void setImmortal(bool b);
    Bullet* shoot();
    std::list<Line*> getLines();

private:
    bool acceleration;
    bool left = false;
    bool right = false;
    float speedX;
    float speedY;
    int ticks = 0;
    int immortalTicks = 0;
    bool immortal = false;
    bool canShoot = false;
};
