#pragma once
#include "Entity.h"
#include "../Color.h"
#include "Bullet.h"

class Ufo :
    public Entity
{
public: 
    Ufo(float x, float y, float dir)
    {
        this->x = x;
        this->y = y;
        this->dir = dir;
        this->speed = 0.11f;
        this->sprite = new Sprite(0.5f, 0.35f);
        this->sprite->addLine(new Line(0.2f, 0.5f, 0.8f, 0.5f, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.8f, 0.5f, 1.f, 0.65f, WHITE_UINT_32));
        this->sprite->addLine(new Line(1.f, 0.65f, 0.8f, 0.8f, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.8f, 0.8f, 0.2f, 0.8f, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.2f, 0.8f, 0.f, 0.65f, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.f, 0.65f, 0.2f, 0.5f, WHITE_UINT_32));

        this->sprite->addLine(new Line(0.35, 0.5, 0.35, 0.1, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.35, 0.1, 0.4, 0, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.4, 0, 0.6, 0, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.6, 0, 0.65, 0.1, WHITE_UINT_32));
        this->sprite->addLine(new Line(0.65, 0.1, 0.65, 0.5, WHITE_UINT_32));
        this->sprite->setSizeFactor(45);
        this->dummy = new Dummy(22.5, 16, 45, 36);
    }
    Bullet* shoot();
    void move(float dt);
private:
    int ticks = 0;
};

