#include "Bullet.h"

bool Bullet::isFriendly()
{
    return this->friendly;
}

bool Bullet::collidesWith(Entity* ent)
{
    if (ent == source) 
    {
        return false;
    }
    else
    {
        return this->Entity::collidesWith(ent);
    }

}
