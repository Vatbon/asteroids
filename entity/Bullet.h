#pragma once
#include "Entity.h"
#include "../Color.h"

class Bullet :
    public Entity
{
public:
    Bullet(float x, float y, float dir, bool friendly, Entity* source)
    {
        this->sprite = new Sprite(0.5, 0);
        if (friendly)
            sprite->addLine(new Line(0, 0, 1, 0, GREEN_UINT_32));
        else
            sprite->addLine(new Line(0, 0, 1, 0, RED_UINT_32));
        sprite->setSizeFactor(20);

        this->dummy = new Dummy(10, 2.5, 20, 5);

        this->x = x;
        this->y = y;
        this->dir = dir;
        this->speed = 0.7;
        this->friendly = friendly;
        this->source = source;
    }

    Bullet(float x, float y, float dir, float speed, bool friendly, Entity* source)
    {
        this->sprite = new Sprite(0.5, 0);
        if (friendly)
            sprite->addLine(new Line(0, 0, 1, 0, GREEN_UINT_32));
        else
            sprite->addLine(new Line(0, 0, 1, 0, RED_UINT_32));
        sprite->setSizeFactor(20);

        this->dummy = new Dummy(10, 2.5, 20, 5);
        this->x = x;
        this->y = y;
        this->dir = dir;
        this->speed = speed;
        this->friendly = friendly;
        this->source = source;
    }

    bool isFriendly();
    bool collidesWith(Entity* ent);

private:
    bool friendly;
    Entity* source = nullptr;
};

