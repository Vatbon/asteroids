#include "Entity.h"
#include <math.h>

constexpr auto PI = 3.14159;

std::list<Line*> Entity::getLines()
{
	if (transformedLines == NULL) {
		transformedLines = new std::list<Line*>();
	}
	else
	{
		clearList(transformedLines);
	}
	std::list<Line*> lines = this->sprite->getLines();
	Point center = *this->sprite->getCenter();
	for (auto iter = lines.begin(); iter != lines.end(); iter++)
	{
		Line *currLine = *iter;
		float s = sinf(this->dir * PI / 180);
		float c = cosf(this->dir * PI / 180);

		float x0 = (currLine->p0->x - center.x) * this->getSizeFactor();
		float y0 = (currLine->p0->y - center.y) * this->getSizeFactor();
		float x1 = (currLine->p1->x - center.x) * this->getSizeFactor();
		float y1 = (currLine->p1->y - center.y) * this->getSizeFactor();

		float nx0 = x0 * c - y0 * s;
		float ny0 = x0 * s + y0 * c;
		float nx1 = x1 * c - y1 * s;
		float ny1 = x1 * s + y1 * c;

		transformedLines->push_back(new Line(nx0 + this->x, ny0 + this->y,
											 nx1 + this->x, ny1 + this->y, currLine->color));
	}
	// DRAW DUMMY LINES
	// Currnetly set to false
	if (false && this->dummy != NULL)
	{
		std::list<Line*> linesDummy = this->dummy->getLines();
		for (auto iter = linesDummy.begin(); iter != linesDummy.end(); iter++)
		{
			Line* currLine = *iter;
			float s = sinf(this->dir * PI / 180);
			float c = cosf(this->dir * PI / 180);

			float x0 = (currLine->p0->x);
			float y0 = (currLine->p0->y);
			float x1 = (currLine->p1->x);
			float y1 = (currLine->p1->y);

			float nx0 = x0 * c - y0 * s;
			float ny0 = x0 * s + y0 * c;
			float nx1 = x1 * c - y1 * s;
			float ny1 = x1 * s + y1 * c;

			transformedLines->push_back(new Line(nx0 + this->x, ny0 + this->y,
				nx1 + this->x, ny1 + this->y, currLine->color));
		}
	}
	return *transformedLines;
}

float Entity::getDir()
{
	return this->dir;
}

void Entity::setDir(float dir)
{
	this->dir = dir;
}

void Entity::setSizeFactor(float factor)
{
	this->sprite->setSizeFactor(factor);
}

float Entity::getSizeFactor()
{
	return this->sprite->getSizeFactor();
}

void Entity::setX(float x)
{
	this->x = x;
}

void Entity::setY(float y)
{
	this->y = y;
}

float Entity::getX()
{
	return this->x;
}

float Entity::getY()
{
	return this->y;
}

float Entity::getSpeed()
{
	return this->speed;
}

void Entity::setSpeed(float speed)
{
	this->speed = speed;
}

void Entity::move(float dt)
{
	this->x += this->speed * cos(this->dir * PI / 180);
	this->y += this->speed * sin(this->dir * PI / 180);
}

bool Entity::collidesWith(Entity* ent)
{
	if (ent != nullptr && this->dummy != nullptr && ent->dummy != nullptr)
	{
		float _x = this->x - ent->x;
		float _y = this->y - ent->y;
		float dist = _x * _x + _y * _y;

		if (sqrtf(dist) - (sqrtf(this->dummy->radiusSqr) + sqrtf(ent->dummy->radiusSqr)) > 0)
		{
			this->dummy->collided = false;
			ent->dummy->collided = false;
			return false;
		}

		return this->dummy->collidesWith(ent->dummy, this->x, this->y, this->dir, ent->x, ent->y, ent->dir);
	}
	else
	{
		return false;
	}
}
