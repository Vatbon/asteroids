#include "Ship.h"
#include "../Color.h"

constexpr auto ANGULAR_SPEED = 350;
constexpr auto LINEAR_ACCELERATION = 1.2;
constexpr auto FRICTION_FACTOR = 1.0;
constexpr auto PI = 3.14159;
constexpr auto EPSILON = 0.0001f;

void Ship::accelerate(bool b)
{
	this->acceleration = b;
}

void Ship::turnLeft(bool b)
{
	this->left = b;
}

void Ship::turnRight(bool b)
{
	this->right = b;
}

void Ship::move(float dt)
{
	if (ticks > 500)
	{
		this->canShoot = true;
		ticks = 0;
	}
	if (immortalTicks > 2000)
	{
		this->immortal = false;
		immortalTicks = 0;
	}
	ticks++;
	immortalTicks++;
	if (left) 
	{
		this->dir -= ANGULAR_SPEED * dt;
	}
	if (right) 
	{
		this->dir += ANGULAR_SPEED * dt;
	}
	this->speedX -= this->speedX * FRICTION_FACTOR * dt;
	this->speedY -= this->speedY * FRICTION_FACTOR * dt;
	if (fabsf(this->speedX) < EPSILON)
	{
		this->speedX = 0;
	}
	if (fabsf(this->speedY) < EPSILON)
	{
		this->speedY = 0;
	}
	if (this->acceleration)
	{
		this->speedX += LINEAR_ACCELERATION * cosf(this->dir * PI / 180) * dt;
		this->speedY += LINEAR_ACCELERATION * sinf(this->dir * PI / 180) * dt;
	}

	this->x += this->speedX;
	this->y += this->speedY;
}

bool Ship::isImmortal()
{
	return this->immortal;
}

void Ship::setImmortal(bool b)
{
	if (!this->immortal && b)
	{
		immortalTicks = 0;
	}
	this->immortal = b;
}

Bullet* Ship::shoot()
{
	if (this->canShoot)
	{
		this->canShoot = false;
		this->ticks = 0;
		return new Bullet(this->x, this->y, this->dir, true, this);
	}
	else
	{
		return nullptr;
	}
}

std::list<Line*> Ship::getLines()
{
	auto lines = this->Entity::getLines();
	if (immortal)
	for (auto iter = lines.begin(); iter != lines.end(); iter++)
	{
		(*iter)->color = DARK_BLUE_UINT_32;
	}
	return lines;
}
