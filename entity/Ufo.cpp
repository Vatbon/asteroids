#include "Ufo.h"

Bullet* Ufo::shoot()
{
	if (this->ticks > 750)
	{
		this->ticks = 0;
		return new Bullet(this->x, this->y, rand(), 0.35, false, this);
	}
	else
	{
		return nullptr;
	}
}

void Ufo::move(float dt)
{
	this->ticks++;
	this->Entity::move(dt);
}