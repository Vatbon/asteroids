#include "Asteroid.h"

void Asteroid::move(float dt)
{
    this->x += this->speed * cosf(this->moveDir * 3.14159 / 180);
    this->y += this->speed * sinf(this->moveDir * 3.14159 / 180);
    this->dir += this->speed;
}

Asteroid* AsteroidFactory::instanse(float x, float y, float dir, float speed, int size)
{
    Sprite* sprite = new Sprite(0.5, 0.5);
    sprite->addLine(new Line(0.3, 0, 0.6, 0.2, WHITE_UINT_32));
    sprite->addLine(new Line(0.6, 0.2, 0.75, 0.05, WHITE_UINT_32));
    sprite->addLine(new Line(0.75, 0.05, 0.9, 0, WHITE_UINT_32));
    sprite->addLine(new Line(0.9, 0, 1, 0.3, WHITE_UINT_32));
    sprite->addLine(new Line(1, 0.3, 0.9, 0.5, WHITE_UINT_32));
    sprite->addLine(new Line(0.9, 0.5, 1, 0.8, WHITE_UINT_32));
    sprite->addLine(new Line(1, 0.8, 0.8, 1, WHITE_UINT_32));
    sprite->addLine(new Line(0.8, 1, 0.6, 0.8, WHITE_UINT_32));
    sprite->addLine(new Line(0.6, 0.8, 0.4, 1, WHITE_UINT_32));
    sprite->addLine(new Line(0.4, 1, 0, 0.7, WHITE_UINT_32));
    sprite->addLine(new Line(0, 0.7, 0.15, 0.4, WHITE_UINT_32));
    sprite->addLine(new Line(0.15, 0.4, 0.05, 0.2, WHITE_UINT_32));
    sprite->addLine(new Line(0.05, 0.2, 0.3, 0, WHITE_UINT_32));

    float sizeFactor = 0.f;
    switch (size)
    {
    case 1:
        sizeFactor = 25.f;
        break;
    case 2:
        sizeFactor = 42.f;
        break;
    case 3:
        sizeFactor = 62.f;
        break;
    }
    sprite->setSizeFactor(sizeFactor);
    Dummy* dummy = new Dummy(sizeFactor / 2, sizeFactor / 2, sizeFactor, sizeFactor);
    Asteroid* ast = new Asteroid(sprite, dummy, x, y, dir, speed, size);
    return ast;
}
