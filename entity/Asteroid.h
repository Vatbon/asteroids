#pragma once
#include "Entity.h"
#include "../Color.h"

class Asteroid :
    public Entity
{
public:
    Asteroid(Sprite* sprite, Dummy* dummy, float x, float y, float moveDir, float speed, int size) {
        this->sprite = sprite;
        this->dummy = dummy;
        this->x = x;
        this->y = y;
        this->dir = 0;
        this->moveDir = moveDir;
        this->speed = speed;
        this->size = size;
    }

    int getSize() { return this->size; }
    void move(float dt);

private:
    float moveDir;
    int size;
};

class AsteroidFactory
{
public:
    static Asteroid* instanse(float x, float y, float dir, float speed, int size);
};

