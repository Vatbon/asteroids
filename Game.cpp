#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include <stdio.h>
#include <ctime>

#include "Engine.h"
#include "Drawer.h"
#include "Menu.h"
#include "Util.h"

#include "entity/Entity.h"
#include "entity/Asteroid.h"
#include "entity/Ship.h"
#include "entity/Bullet.h"
#include "entity/Ufo.h"

constexpr auto GAMESTATE_MENU = 0;
constexpr auto GAMESTATE_ACTION = 1;

Ship* ship = NULL;
Entity* title = NULL;
Entity* subTitle = NULL;
Entity* score = NULL;
Entity* livesEnt = NULL;
Entity* highestEnt = NULL;
std::list<Asteroid*>* asteroids = new std::list<Asteroid*>;
std::list<Bullet*>* bullets = new std::list<Bullet*>;
Ufo* ufo = NULL;
std::list<Drawable*>* toDraw = new std::list<Drawable*>;

int scoreInt = 0;
int highestScore = -1;
int lives = 3;
bool scoreChanged = true;
bool livesChanged = true;
bool asteroidSpawned = false;
int gameState = 0;
volatile int ticksPassed = 0;

float rand(float min, float max)
{
    return min + (rand() * 1.0f / RAND_MAX) * (max - min);
}

Asteroid* createAsteroid()
{
    int side = rand() % 4;
    int size = 1 + rand() % 3;
    int spray = 20;
    if (side == 0) // right
    {
        return AsteroidFactory::instanse(SCREEN_WIDTH, rand(10, SCREEN_HEIGHT - 10), rand(180 - spray, 180 + spray), rand(0.1, 0.2), size);
    }
    else if (side == 1) // bottom
    {
        return AsteroidFactory::instanse(rand(10, SCREEN_WIDTH - 10), SCREEN_HEIGHT, rand(270 - spray, 270 + spray), rand(0.1, 0.2), size);
    }
    else if (side == 2) // left
    {
        return AsteroidFactory::instanse(0, rand(10, SCREEN_HEIGHT - 10), rand(0 - spray, 0 + spray), rand(0.1, 0.2), size);
    }
    else // top
    {
        return AsteroidFactory::instanse(rand(10, SCREEN_WIDTH - 10), 0, rand(90 - spray, 90 + spray), rand(0.1, 0.2), size);
    }
}

void destroyAsteroid(Asteroid* asteroid)
{
    if (asteroid->getSize() == 1)
    {
        return;
    }
    int count = rand() % 3;
    int newSize = asteroid->getSize() - 1;
    for (int i = 0; i < count; i++)
    {
        asteroids->push_back(AsteroidFactory::instanse(asteroid->getX(), asteroid->getY(), rand(), rand(0.1, 0.2), newSize));
    }
}

template <class C> void moveEntities(std::list<C*>* movables, float dt)
{
    for (auto movable = movables->begin(); movable != movables->end();)
    {
        C* ent = *movable;
        if (ent->getX() < -100 || ent->getX() > SCREEN_WIDTH + 100 || ent->getY() < -100 || ent->getY() > SCREEN_HEIGHT + 100)
        {
            delete ent;
            movable = movables->erase(movable);
        }
        else {
            ent->move(dt);
            toDraw->push_back(ent);
            movable++;
        }
    }
}

void check_collision()
{
    if (ship != nullptr && !ship->isImmortal())
    {
        for (auto asteroid = asteroids->begin(); asteroid != asteroids->end(); asteroid++)
        {
            if (ship->collidesWith(*asteroid))
            {
                lives--;
                livesChanged = true;
                ship->setImmortal(true);
            }
        }
        for (auto bullet = bullets->begin(); bullet != bullets->end(); bullet++)
        {
            if (!(*bullet)->isFriendly() && (*bullet)->collidesWith(ship))
            {
                delete* bullet;
                bullet = bullets->erase(bullet);
                lives--;
                livesChanged = true;
                ship->setImmortal(true);
            }
        }
    }

    for (auto asteroid = asteroids->begin(); asteroid != asteroids->end();)
    {
        bool wasImpact = false;
        for (auto bullet = bullets->begin(); bullet != bullets->end(); bullet ++)
        {
            if ((*bullet)->isFriendly() && (*bullet)->collidesWith(*asteroid))
            {
                scoreChanged = true;
                scoreInt += 10;
                wasImpact = true;
                destroyAsteroid(*asteroid);

                delete* bullet;
                bullet = bullets->erase(bullet);
                delete* asteroid;
                asteroid = asteroids->erase(asteroid);
                break;
            }
        }
        if (!wasImpact) asteroid++;
    }

    if (ufo != nullptr)
        for (auto bullet = bullets->begin(); bullet != bullets->end();)
        {
            bool wasImpact = false;
            if ((*bullet)->collidesWith(ufo))
            {
                scoreChanged = true;
                scoreInt += 200;
                delete* bullet;
                bullet = bullets->erase(bullet);
                delete ufo;
                ufo = nullptr;
                break;
            }
            if (!wasImpact) bullet++;
        }
}

void finishGame()
{
    delete ship;
    ship = nullptr;
    gameState = GAMESTATE_MENU;
    if (scoreInt > highestScore)
    {
        highestScore = scoreInt;
    }
}

void startGame()
{
    gameState = GAMESTATE_ACTION;
    ship = new Ship(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 0);
    clearList(asteroids);
    clearList(bullets);
    delete ufo;
    ufo = nullptr;
    scoreInt = 0;
    lives = 3;
    delete highestEnt;
    highestEnt = nullptr;
    ticksPassed = 0;
    asteroidSpawned = false;
}

void initialize()
{
    srand(time(0));
    title = createTitle();
    title->setSizeFactor(150);
    title->setX(SCREEN_WIDTH / 2);
    title->setY(SCREEN_HEIGHT * 0.384);

    subTitle = createSubTitle();
    subTitle->setSizeFactor(50);
    subTitle->setX(SCREEN_WIDTH / 2);
    subTitle->setY(SCREEN_WIDTH * 0.618);

    gameState = GAMESTATE_MENU;
    ufo = new Ufo(-50, SCREEN_WIDTH / 2, 0);
}

void act(float dt)
{
    if (!is_window_active())
    {
        return;
    }

    if (is_key_pressed(VK_ESCAPE))
    {
        schedule_quit_game();
    }

    toDraw->clear();
    ticksPassed++;

    if (lives < 1 || asteroids->empty() && asteroidSpawned && ufo == nullptr)
    {
        finishGame();
    }

    if (gameState == GAMESTATE_ACTION && scoreChanged)
    {
        delete score;
        score = makeScore(scoreInt);
        score->setX(5);
        score->setY(5);
        scoreChanged = false;
    }

    if (gameState == GAMESTATE_ACTION && livesChanged)
    {
        delete livesEnt;
        livesEnt = makeLives(lives);
        livesEnt->setX(SCREEN_WIDTH - 120);
        livesEnt->setY(5);
        livesChanged = false;
    }

    if (gameState == GAMESTATE_MENU && highestScore > 0 && highestEnt == nullptr)
    {
        highestEnt = makeHighestScore(highestScore);
        highestEnt->setX(SCREEN_WIDTH / 2);
        highestEnt->setY(SCREEN_HEIGHT / 2);
    }

    if (ticksPassed % 250 == 0)
    {
        scoreChanged = true;
        scoreInt += 1;
    }

    if (ticksPassed % 1600 == 0)
    {
        asteroids->push_back(createAsteroid());
        asteroidSpawned = true;
    }

    if (ticksPassed == 19999)
    {
        ticksPassed = 0;
        if (ufo == nullptr)
        {
            ufo = new Ufo(-50, rand(100, SCREEN_HEIGHT - 100), 0);
        }
    }

    if (gameState == GAMESTATE_MENU && is_key_pressed(VK_SPACE))
    {
        startGame();
    }

    check_collision();
    moveEntities(asteroids, dt);
    moveEntities(bullets, dt);

    if (ship != nullptr)
    {
        ship->move(dt);
        ship->accelerate(is_key_pressed(VK_UP));
        ship->turnLeft(is_key_pressed(VK_LEFT));
        ship->turnRight(is_key_pressed(VK_RIGHT));
        if (ship->getX() > SCREEN_WIDTH + 20)
        {
            ship->setX(-20);
        }
        else if (ship->getX() < -20)
        {
            ship->setX(SCREEN_WIDTH + 20);
        }
        else if (ship->getY() > SCREEN_HEIGHT + 20)
        {
            ship->setY(-20);
        }
        else if (ship->getY() < -20)
        {
            ship->setY(SCREEN_HEIGHT + 20);
        }
        if (is_key_pressed(VK_SPACE)) {
            Bullet* bullet = ship->shoot();
            if (bullet != nullptr)
            {
                bullets->push_back(bullet);
            }
        }
        toDraw->push_back(ship);
    }

    if (ufo != nullptr)
    {
        ufo->move(dt);
        Bullet* bullet = ufo->shoot();
        if (bullet != nullptr)
        {
            bullets->push_back(bullet);
        }
        if (ufo->getX() > SCREEN_WIDTH + 20)
        {
            delete ufo;
            ufo = nullptr;
        }
        toDraw->push_back(ufo);
    }

    if (gameState == GAMESTATE_MENU)
    {
        toDraw->push_back(title);
        toDraw->push_back(highestEnt);
        toDraw->push_back(subTitle);
    }
    else if (gameState == GAMESTATE_ACTION)
    {
        toDraw->push_back(score);
        toDraw->push_back(livesEnt);
    }
}

void draw()
{
    if (is_window_active)
    {
        clear_buffer();
        for (auto drawable = toDraw->begin(); drawable != toDraw->end(); drawable++)
        {
            draw(*drawable);
        }
    }
}

void finalize()
{
    delete ship;
    delete title;
    delete subTitle;
    delete score;
    delete livesEnt;
    delete highestEnt;
    clearList(asteroids);
    clearList(bullets);
    delete asteroids;
    delete bullets;
    delete ufo;
    toDraw->clear();
    delete toDraw;
}
