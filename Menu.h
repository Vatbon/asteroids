#pragma once

#include "entity/Entity.h"

Entity* createTitle();
Entity* createSubTitle();
Entity* makeScore(int score);
Entity* makeLives(int lives);
Entity* makeHighestScore(int score);