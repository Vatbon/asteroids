#include "Menu.h"
#include "Color.h"

Entity* createTitle()
{
    Sprite* spriteTitle = new Sprite(2.2, 0.3);
    //A
    spriteTitle->addLine(new Line(0, 0.6, 0, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0, 0.3, 0.2, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0.2, 0, 0.4, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32));
    //S
    spriteTitle->addLine(new Line(0.5, 0, 0.9, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0.5, 0, 0.5, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0.5, 0.3, 0.9, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0.9, 0.3, 0.9, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(0.9, 0.6, 0.5, 0.6, WHITE_UINT_32));
    //T
    spriteTitle->addLine(new Line(1, 0, 1.4, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(1.2, 0, 1.2, .6, WHITE_UINT_32));
    //E
    spriteTitle->addLine(new Line(1.5, 0, 1.9, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(1.5, 0.3, 1.9, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(1.5, 0.6, 1.9, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(1.5, 0, 1.5, .6, WHITE_UINT_32));
    //R
    spriteTitle->addLine(new Line(2, 0.6, 2, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(2, 0, 2.4, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(2.4, 0, 2.4, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(2.4, 0.3, 2, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(2, 0.3, 2.4, 0.6, WHITE_UINT_32));
    //O
    spriteTitle->addLine(new Line(2.5, 0, 2.9, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(2.9, 0, 2.9, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(2.9, 0.6, 2.5, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(2.5, 0.6, 2.5, 0, WHITE_UINT_32));
    //I
    spriteTitle->addLine(new Line(3, 0, 3.4, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(3, 0.6, 3.4, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(3.2, 0, 3.2, 0.6, WHITE_UINT_32));
    //D
    spriteTitle->addLine(new Line(3.5, 0, 3.7, 0.0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(3.7, 0, 3.9, 0.2, WHITE_UINT_32));
    spriteTitle->addLine(new Line(3.9, 0.2, 3.9, 0.4, WHITE_UINT_32));
    spriteTitle->addLine(new Line(3.9, 0.4, 3.7, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(3.7, 0.6, 3.5, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(3.5, 0.6, 3.5, 0, WHITE_UINT_32));
    //S
    spriteTitle->addLine(new Line(4.4, 0, 4, 0, WHITE_UINT_32));
    spriteTitle->addLine(new Line(4, 0, 4, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(4, 0.3, 4.4, 0.3, WHITE_UINT_32));
    spriteTitle->addLine(new Line(4.4, 0.3, 4.4, 0.6, WHITE_UINT_32));
    spriteTitle->addLine(new Line(4.4, 0.6, 4, 0.6, WHITE_UINT_32));

    return new Entity(spriteTitle);
}

Entity* createSubTitle()
{
    Sprite* subTitleSprite = new Sprite(4.7, 0.3);
    //P
    subTitleSprite->addLine(new Line(0, 0, 0.4, 0, WHITE_UINT_32));
    subTitleSprite->addLine(new Line(0.4, 0, 0.4, 0.3, WHITE_UINT_32));
    subTitleSprite->addLine(new Line(0.4, 0.3, 0, 0.3, WHITE_UINT_32));
    subTitleSprite->addLine(new Line(0, 0, 0, 0.6, WHITE_UINT_32));
    //R
    subTitleSprite->addLine((new Line(0, 0.6, 0, 0, WHITE_UINT_32))->move(.5, 0));
    subTitleSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0, 0.4, 0.3, WHITE_UINT_32))->move(.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0, 0.3, WHITE_UINT_32))->move(.5, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(.5, 0));
    //E
    subTitleSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(1, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(1, 0));
    subTitleSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(1, 0));
    subTitleSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(1, 0));
    //S
    subTitleSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(1.5, 0));
    subTitleSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(1.5, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(1.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(1.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(1.5, 0));
    //S
    subTitleSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(2, 0));
    subTitleSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(2, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(2, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(2, 0));
    subTitleSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(2, 0));
    //S
    subTitleSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(3, 0));
    subTitleSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(3, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(3, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
    subTitleSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(3, 0));
    //P
    subTitleSprite->addLine((new Line(0, 0.6, 0, 0, WHITE_UINT_32))->move(3.5, 0));
    subTitleSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(3.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0, 0.4, 0.3, WHITE_UINT_32))->move(3.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0, 0.3, WHITE_UINT_32))->move(3.5, 0));
    //A
    subTitleSprite->addLine((new Line(0, 0.6, 0, 0.3, WHITE_UINT_32))->move(4, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.2, 0, WHITE_UINT_32))->move(4, 0));
    subTitleSprite->addLine((new Line(0.2, 0, 0.4, 0.3, WHITE_UINT_32))->move(4, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(4, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(4, 0));
    //C
    subTitleSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(4.5, 0));
    subTitleSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(4.5, 0));
    subTitleSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(4.5, 0));
    //E
    subTitleSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(5, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(5, 0));
    subTitleSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(5, 0));
    subTitleSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(5, 0));
    //T
    subTitleSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(6, 0));
    subTitleSprite->addLine((new Line(0.2, 0, 0.2, 0.6, WHITE_UINT_32))->move(6, 0));
    //O
    subTitleSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(6.5, 0));
    subTitleSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(6.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0.6, 0.4, 0, WHITE_UINT_32))->move(6.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(6.5, 0));
    //P
    subTitleSprite->addLine((new Line(0, 0.6, 0, 0, WHITE_UINT_32))->move(7.5, 0));
    subTitleSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(7.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0, 0.4, 0.3, WHITE_UINT_32))->move(7.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0, 0.3, WHITE_UINT_32))->move(7.5, 0));
    //L
    subTitleSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(8, 0));
    subTitleSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(8, 0));
    //A
    subTitleSprite->addLine((new Line(0, 0.6, 0, 0.3, WHITE_UINT_32))->move(8.5, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.2, 0, WHITE_UINT_32))->move(8.5, 0));
    subTitleSprite->addLine((new Line(0.2, 0, 0.4, 0.3, WHITE_UINT_32))->move(8.5, 0));
    subTitleSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(8.5, 0));
    subTitleSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(8.5, 0));
    //Y
    subTitleSprite->addLine((new Line(0, 0, 0.2, 0.3, WHITE_UINT_32))->move(9, 0));
    subTitleSprite->addLine((new Line(0.2, 0.3, 0.4, 0, WHITE_UINT_32))->move(9, 0));
    subTitleSprite->addLine((new Line(0.2, 0.3, 0.2, 0.6, WHITE_UINT_32))->move(9, 0));

    return new Entity(subTitleSprite);
}

Entity* makeScore(int score)
{
    Sprite* scoreSprite = new Sprite(0, 0);
    //S
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(0, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(0, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(0, 0));
    scoreSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(0, 0));
    scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(0, 0));
    //C
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(0.5, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(0.5, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(0.5, 0));
    //O
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(1, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(1, 0));
    scoreSprite->addLine((new Line(0.4, 0.6, 0.4, 0, WHITE_UINT_32))->move(1, 0));
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(1, 0));
    //R
    scoreSprite->addLine((new Line(0, 0.6, 0, 0, WHITE_UINT_32))->move(1.5, 0));
    scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(1.5, 0));
    scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.3, WHITE_UINT_32))->move(1.5, 0));
    scoreSprite->addLine((new Line(0.4, 0.3, 0, 0.3, WHITE_UINT_32))->move(1.5, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(1.5, 0));
    //E
    scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(2, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(2, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(2, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(2, 0));
    float shift = 3.f;
    int nums[10] = { -1 };
    int pos = 0;
    do  {
        nums[pos] = score % 10;
        score /= 10;
        pos++;
    } while (score > 0);
    pos--;
    while (pos >= 0) {
        switch (nums[pos])
        {
        case 0:
            scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(shift , 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 1:
            scoreSprite->addLine((new Line(0, 0.2, 0.2, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.2, 0, 0.2, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 2:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.31, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.3, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 3:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 4:
            scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 5:
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 6:
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 7:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 8:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(shift, 0));    
            break;
        case 9:
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(shift, 0)); 
            break;
        }
        shift += 0.5f;
        pos--;
    }
    scoreSprite->setSizeFactor(30);
    return new Entity(scoreSprite);
}

Entity* makeLives(int lives)
{
    Sprite* livesSprite = new Sprite(0, 0);
    //L
    livesSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(0, 0));
    livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(0, 0));
    //I
    livesSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(0.5, 0));
    livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(0.5, 0));
    livesSprite->addLine((new Line(0.2, 0, 0.2, 0.6, WHITE_UINT_32))->move(0.5, 0));
    //V
    livesSprite->addLine((new Line(0, 0, 0.2, 0.6, WHITE_UINT_32))->move(1, 0));
    livesSprite->addLine((new Line(0.2, 0.6, 0.4, 0, WHITE_UINT_32))->move(1, 0));
    //E
    livesSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(1.5, 0));
    livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(1.5, 0));
    livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(1.5, 0));
    livesSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(1.5, 0));
    //S
    livesSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(2, 0));
    livesSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(2, 0));
    livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(2, 0));
    livesSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(2, 0));
    livesSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(2, 0));

    switch (lives)
    {
    case 0:
        livesSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0.6, 0.4, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 1:
        livesSprite->addLine((new Line(0, 0.2, 0.2, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.2, 0, 0.2, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 2:
        livesSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0, 0.4, 0.31, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0.3, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 3:
        livesSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 4:
        livesSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 5:
        livesSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 6:
        livesSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 7:
        livesSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 8:
        livesSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    case 9:
        livesSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(3, 0));
        livesSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(3, 0));
        break;
    }

    livesSprite->setSizeFactor(30);
    return new Entity(livesSprite);
}

Entity* makeHighestScore(int score)
{
    Sprite* scoreSprite = new Sprite((14 * 0.5 - 0.1 + log10l(score) * 0.5) / 2, 0.3);
    //H
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(0, 0));
    scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(0, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(0, 0));
    //I
    scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(0.5, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(0.5, 0));
    scoreSprite->addLine((new Line(0.2, 0, 0.2, 0.6, WHITE_UINT_32))->move(0.5, 0));
    //G
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(1, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(1, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(1, 0));
    scoreSprite->addLine((new Line(0.4, 0.6, 0.4, 0.3, WHITE_UINT_32))->move(1, 0));
    scoreSprite->addLine((new Line(0.4, 0.3, 0.2, 0.3, WHITE_UINT_32))->move(1, 0));
    //H
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(1.5, 0));
    scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(1.5, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(1.5, 0));
    //E
    scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(2, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(2, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(2, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(2, 0));
    //S
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(2.5, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(2.5, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(2.5, 0));
    scoreSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(2.5, 0));
    scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(2.5, 0));
    //T 
    scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(3, 0));
    scoreSprite->addLine((new Line(0.2, 0.6, 0.2, 0, WHITE_UINT_32))->move(3, 0));

    //S
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(4, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(4, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(4, 0));
    scoreSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(4, 0));
    scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(4, 0));
    //C
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(4.5, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(4.5, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(4.5, 0));
    //O
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(5, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(5, 0));
    scoreSprite->addLine((new Line(0.4, 0.6, 0.4, 0, WHITE_UINT_32))->move(5, 0));
    scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(5, 0));
    //R
    scoreSprite->addLine((new Line(0, 0.6, 0, 0, WHITE_UINT_32))->move(5.5, 0));
    scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(5.5, 0));
    scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.3, WHITE_UINT_32))->move(5.5, 0));
    scoreSprite->addLine((new Line(0.4, 0.3, 0, 0.3, WHITE_UINT_32))->move(5.5, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(5.5, 0));
    //E
    scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(6, 0));
    scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(6, 0));
    scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(6, 0));
    scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(6, 0));
    float shift = 7.f;
    int nums[10] = { -1 };
    int pos = 0;
    do {
        nums[pos] = score % 10;
        score /= 10;
        pos++;
    } while (score > 0);
    pos--;
    while (pos >= 0) {
        switch (nums[pos])
        {
        case 0:
            scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 1:
            scoreSprite->addLine((new Line(0, 0.2, 0.2, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.2, 0, 0.2, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 2:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.31, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.3, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 3:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 4:
            scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 5:
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 6:
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.3, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 7:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 8:
            scoreSprite->addLine((new Line(0, 0, 0.4, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.6, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        case 9:
            scoreSprite->addLine((new Line(0.4, 0, 0, 0, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0, 0, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0, 0.3, 0.4, 0.3, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0, 0.4, 0.6, WHITE_UINT_32))->move(shift, 0));
            scoreSprite->addLine((new Line(0.4, 0.6, 0, 0.6, WHITE_UINT_32))->move(shift, 0));
            break;
        }
        shift += 0.5f;
        pos--;
    }
    scoreSprite->setSizeFactor(45);
    return new Entity(scoreSprite);
}
