#include "Dummy.h"
#include "../Color.h"
#include <math.h>

std::list<Line*> Dummy::getLines()
{
	if (transformedLines == NULL) {
		transformedLines = new std::list<Line*>();
	}
	else
	{
		clearList(transformedLines);
	}
	uint32_t color = GREEN_UINT_32;
	if (this->collided)
	{
		color = RED_UINT_32;
	}
	transformedLines->push_front((new Line(0, 0, 0, this->h, color))->move(-this->x, -this->y));
	transformedLines->push_front((new Line(0, this->h, this->w, this->h, color))->move(-this->x, -this->y));
	transformedLines->push_front((new Line(this->w, this->h, this->w, 0, color))->move(-this->x, -this->y));
	transformedLines->push_front((new Line(this->w, 0, 0, 0, color))->move(-this->x, -this->y));
    return *transformedLines;
}

bool inside(float x, float y, float w, float h)
{
	return -w < x&& x < w && -h < y&& y < h;
}

bool collides(Dummy* dummy1, float x1, float y1, float dir1, Dummy* dummy2, float x2, float y2, float dir2)
{
	x1 -= x2;
	y1 -= y2;

	float s = sinf(((float)dir1 - (float)dir2) * 3.1415f / 180);
	float c = cosf(((float)dir1 - (float)dir2) * 3.1415f / 180);

	float _x0 = 0 - dummy1->x;
	float _x1 = dummy1->w - dummy1->x;
	float _y0 = 0 - dummy1->y;
	float _y1 = dummy1->h - dummy1->y;

	float v0x = _x0 * c - _y0 * s + x1;
	float v0y = _x0 * s + _y0 * c + y1;

	float v1x = _x1 * c - _y0 * s + x1;
	float v1y = _x1 * s + _y0 * c + y1;

	float v2x = _x0 * c - _y1 * s + x1;
	float v2y = _x0 * s + _y1 * c + y1;

	float v3x = _x1 * c - _y1 * s + x1;
	float v3y = _x1 * s + _y1 * c + y1;

	if (inside(v0x, v0y, dummy2->w - dummy2->x, dummy2->h - dummy2->y))
		return true;
	else if (inside(v1x, v1y, dummy2->w - dummy2->x, dummy2->h - dummy2->y))
		return true;
	else if (inside(v2x, v2y, dummy2->w - dummy2->x, dummy2->h - dummy2->y))
		return true;
	else if (inside(v3x, v3y, dummy2->w - dummy2->x, dummy2->h - dummy2->y))
		return true;
	else
		return false;
}

bool Dummy::collidesWith(Dummy* dummy, float x1, float y1, float dir1, float x2, float y2, float dir2)
{
	if (collides(this, x1, y1, dir1, dummy, x2, y2, dir2))
		return true;
	if (collides(dummy, x2, y2, dir2, this, x1, y1, dir1))
		return true;
	return false;
}

bool Dummy::isCollided()
{
	return this->collided;
}
