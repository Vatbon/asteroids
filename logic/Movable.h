#pragma once

class Movable
{
public:
	virtual void move(float dt) = 0;
};