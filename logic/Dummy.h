#pragma once

#include "../Util.h"
#include "../drawable/Drawable.h"

class Dummy : 
	public Drawable
{
public:
	Dummy(float x, float y, float w, float h)
	{
		this->x = x;
		this->y = y;
		this->w = w;
		this->h = h;
		this->radiusSqr = (w * w + h * h) / 2;
	}

	~Dummy()
	{
		if (transformedLines != NULL)
			clearList(transformedLines);
		delete transformedLines;
	}

	std::list<Line*> getLines();
	bool collidesWith(Dummy* dummy, float x1, float y1, float dir1, float x2, float y2, float dir2);
	bool isCollided();

	float x;
	float y;
	float w;
	float h;
	float radiusSqr;
	bool collided = false;

private:
	std::list<Line*>* transformedLines = NULL;
};

