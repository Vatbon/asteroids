#pragma once

#ifndef WHITE_UINT_32
#	define WHITE_UINT_32 0xffffffff
#	define RED_UINT_32 0xffff0000
#	define GREEN_UINT_32 0xff00ff00
#	define BLUE_UINT_32 0xff0000ff
#	define DARK_BLUE_UINT_32 0xff0000f0
#	define MAGENTA_UINT_32 0xffff00ff
#	define YELLOW_UINT_32 0xffffff00
#	define CYAN_UINT_32 0xff00ffff
#endif