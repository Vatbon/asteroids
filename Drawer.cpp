#include "Drawer.h"
#include "Engine.h"
#include <math.h>

template <typename T>
int sign(T x)
{
    if (x > 0) {
        return 1;
    }
    else if (x < 0) {
        return -1;
    }
    else {
        return 0;
    }
}

float absf(float x)
{
    return x > 0 ? x : -x;
}

void intoBuffer(int x, int y, uint32_t value) 
{
    if (0 <= x && x < SCREEN_WIDTH && 0 <= y && y < SCREEN_HEIGHT)
        buffer[y][x] = value;
}

void drawLine(float x0, float y0, float x1, float y1, uint32_t value)
{
    int deltaX = static_cast<int>(absf(roundf(x0 - x1)));
    int deltaY = static_cast<int>(absf(roundf(y0 - y1)));
    int error = 0;

    if (deltaX < deltaY) {
        if (y0 > y1) {
            float temp = x1;
            x1 = x0;
            x0 = temp;
            temp = y1;
            y1 = y0;
            y0 = temp;
        }
        int deltaErr = deltaX + 1;
        int x = static_cast<int>(x0);
        int dirX = static_cast<int>(sign(x1 - x0));
        for (int y = y0; y < y1; y++) {
            intoBuffer(x, y, value);
            error += deltaErr;
            while (error >= (deltaY + 1)) {
                x += dirX;
                error -= deltaY + 1;
            }
        }
    }
    else {
        if (x0 > x1) {
            float temp = x1;
            x1 = x0;
            x0 = temp;
            temp = y1;
            y1 = y0;
            y0 = temp;
        }
        int deltaErr = deltaY + 1;
        int y = static_cast<int>(y0);
        int dirY = static_cast<int>(sign(y1 - y0));
        for (int x = x0; x < x1; x++) {
            intoBuffer(x, y, value);
            error += deltaErr;
            while (error >= (deltaX + 1)) {
                y += dirY;
                error -= deltaX + 1;
            }
        }
    }
}

void draw(Drawable *drawable)
{
    if (drawable == nullptr)
    {
         return;
    }
    auto lines = drawable->getLines();
    for (auto iter = lines.begin(); iter != lines.end(); iter++) 
    {
       drawLine((*iter)->p0->x, (*iter)->p0->y, (*iter)->p1->x, (*iter)->p1->y, (*iter)->color);
    }
}