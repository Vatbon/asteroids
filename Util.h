#pragma once
#include <list>

template <typename T> void clearList(std::list<T*>* list)
{
    while (!list->empty())
    {
        delete list->front();
        list->pop_front();
    }
}