#pragma once

#include "Line.h"
#include <list>

class Drawable
{
	public:
		virtual std::list<Line*> getLines() = 0;
};
