#include "Sprite.h"

void Sprite::addLine(Line *line)
{
    this->lines->push_back(line);
}

std::list<Line*> Sprite::getLines()
{
    return *lines;
}

void Sprite::setSizeFactor(const float f)
{
    this->sizeFactor = f;
}

float Sprite::getSizeFactor()
{
    return this->sizeFactor;
}

Point* Sprite::getCenter()
{
    return center;
}
