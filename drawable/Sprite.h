#pragma once

#include <list>
#include "../Util.h"
#include "Line.h"
#include "Point.h"
#include "Drawable.h"

class Sprite : public Drawable
{
public:

	Sprite(float x, float y)
	{
		this->center = new Point(x, y);
		lines = new std::list<Line*>();
	}

	Sprite() 
	{
		this->center = new Point(0, 0);
		lines = new std::list<Line*>();
	}

	~Sprite()
	{
		clearList(lines);
		delete lines;
		delete center;
	}

	void addLine(Line *line);
	virtual std::list<Line*> getLines();
	void setSizeFactor(const float f);
	float getSizeFactor();
	Point* getCenter();

private:
	std::list<Line*> *lines;
	Point* center;
	float sizeFactor = 1;
};

