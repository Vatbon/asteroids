#pragma once

#include "Point.h"
#include <list>

class Line
{
	public:
		Point *p0;
		Point *p1;
		uint32_t color;

		Line(Point p0, Point p1, uint32_t color)
		{
			this->p0 = &p0;
			this->p1 = &p1;
			this->color = color;
		}

		Line(float x0, float y0, float x1, float y1, uint32_t color)
		{
			this->p0 = new Point(x0, y0);
			this->p1 = new Point(x1, y1);
			this->color = color;
		}

		~Line()
		{
			delete p0;
			delete p1;
		}

		Line* move(float x, float y)
		{
			p0->x += x;
			p0->y += y;
			p1->x += x;
			p1->y += y;
			return this;
		}
};

